require('./bootstrap');

require('@fortawesome/fontawesome-free/js/all');
window.toastr = require('toastr');
window.Vue = require('vue');


Vue.component('nearby-shops', require('./views/NearbyShops.vue').default);
Vue.component('prefered-shops', require('./views/PreferedShops.vue').default);

const app = new Vue({
    el: '#app'
});

toastr.options = {
  	"positionClass": "toast-bottom-right",
}