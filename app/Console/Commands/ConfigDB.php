<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Illuminate\Support\Facades\DB;

class ConfigDB extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'config:db';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Migrate & seed default data';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        // Check DB Connection
        $connected = false;
        try{
            $this->info('Trying to connect to DB...');
            DB::connection('mongodb')->getMongoClient()->listDatabases();
            $connected = true;
        } 
        catch (\MongoDB\Driver\Exception\ConnectionTimeoutException $mongoException) {}
        catch (\MongoDB\Exception\InvalidArgumentException $mongoException) {}

        if($connected){
            // Migrate
            $this->info('Migration...');
            $this->call('migrate');

            // Seed
            $this->info('Seeding...');
            $this->call('db:seed');

            // Install Passport keys
            $this->info('Installing Passport...');
            $this->call('fix:passport');
            $this->call('passport:install');

            $this->info('Congratulation! You are ready to start.');
        }else{
            $this->error('DB Connection Failed: '.$mongoException->getMessage().' Check your DB Connection or your .env file then run : php artisan migrate && php artisan db:seed');
        }
    }
}
