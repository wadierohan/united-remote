<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Illuminate\Support\Facades\Artisan;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\File;

class ConfigApp extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'config:app';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Create .env file';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        // Because being original is great ;)
        $this->info('          _____                    _____          ');
        $this->info('         /\    \                  /\    \         ');
        $this->info('        /::\____\                /::\    \        ');
        $this->info('       /:::/    /               /::::\    \       ');
        $this->info('      /:::/   _/___            /::::::\    \      ');
        $this->info('     /:::/   /\    \          /:::/\:::\    \     ');
        $this->info('    /:::/   /::\____\        /:::/__\:::\    \    ');
        $this->info('   /:::/   /:::/    /       /::::\   \:::\    \   ');
        $this->info('  /:::/   /:::/   _/___    /::::::\   \:::\    \  ');
        $this->info(' /:::/___/:::/   /\    \  /:::/\:::\   \:::\    \ ');
        $this->info('|:::|   /:::/   /::\____\/:::/__\:::\   \:::\____\ ');
        $this->info('|:::|__/:::/   /:::/    /\:::\   \:::\   \::/    /');
        $this->info(' \:::\/:::/   /:::/    /  \:::\   \:::\   \/____/ ');
        $this->info('  \::::::/   /:::/    /    \:::\   \:::\    \     ');
        $this->info('   \::::/___/:::/    /      \:::\   \:::\____\    ');
        $this->info('    \:::\__/:::/    /        \:::\   \::/    /    ');
        $this->info('     \::::::::/    /          \:::\   \/____/     ');
        $this->info('      \::::::/    /            \:::\    \         ');
        $this->info('       \::::/    /              \:::\____\        ');
        $this->info('        \::/____/                \::/    /        ');
        $this->info('         ~~                       \/____/         ');
        $this->info('                                                  ');


        // Check if env file exists, and if user is sure to override it.
        if(!File::exists(base_path('.env')) || (File::exists(base_path('.env')) && $this->confirm('Environment File already exists. Are you sure to override it?'))){

            // Init all Variables
            $config['APP_NAME'] = $this->ask('What is your App name?');
            $config['APP_ENV'] = $this->choice('What is your App env?', [1 => 'local', 2 => 'production'], 'production');
            $APP_DEBUG = $this->confirm('Enable Debug?');
            $APP_DEBUG ? $config['APP_DEBUG'] = "true" : $config['APP_DEBUG'] = "false";
            $config['APP_URL'] = $this->ask('What is your App url?', 'http://localhost');
            $config['DB_HOST'] = $this->ask('What is your DB Host?', '127.0.0.1');
            $config['DB_PORT'] = $this->ask('What is your DB Port?', '27017');
            $config['DB_DATABASE'] = $this->ask('What is your DB Name?', 'admin');
            $config['DB_USERNAME'] = $this->ask('What is your DB Username?');
            $config['DB_PASSWORD'] = $this->secret('What is your DB Password?');

            // Check if any config has a space
            foreach($config as $key => $val){
                if(strpos($val, ' ') !== false){
                    $config[$key] = '"'.$val.'"';
                }
            }

            // Create Env File
            $this->info('Creating .env file...');
            if(File::copy(base_path('.env.example'),base_path('.env'))){
                file_put_contents(
                    base_path('.env'), 
                    str_replace(
                        array_map(function($val){return '{{'.$val.'}}';}, array_keys($config)), 
                        $config, 
                        file_get_contents(base_path('.env'))
                    )
                );
            }

            // Generate new Key
            $this->info('Generating Key...');
            $this->call('key:generate');

            // Clear Config
            $this->call('config:cache');

            $this->info('Next step => kindly run : php artisan config:db');
        }
    }
}
