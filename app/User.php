<?php

namespace App;

use App\Shop;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Notifications\Notifiable;
use Jenssegers\Mongodb\Auth\User as Authenticatable;
use Laravel\Passport\HasApiTokens;

class User extends Authenticatable
{
    use Notifiable, HasApiTokens;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password', 'location'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    public function preferedShops(){
        return $this->belongsToMany(Shop::class, null, 'preferedby', 'preferedshops');
    }

    public function dislikedShops(){
        return $this->belongsToMany(Shop::class, null, 'dislikedby', 'dislikedshops');
    }

    /**
     *
     * Get nearby Shops (I think It would be better to add a maxDistance because this approach is like sorting the near one)
     * Don't select Prefered Shops and Disliked Shops.
     */
    public function nearbyShops(){
        return Shop::where('location', 'near', [
            '$geometry' => $this->location
        ])
        ->whereNotIn('_id', $this->preferedShops()->get()->pluck('_id')->toArray())
        ->whereNotIn('_id', $this->dislikedShops()->get()->pluck('_id')->toArray());
    }

}
