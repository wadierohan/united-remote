<?php

namespace App\Jobs;

use App\Shop;
use App\User;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;

class DeleteShopFromDislike implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    private $user, $shop;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct(User $user, Shop $shop)
    {
        $this->user = $user;
        $this->shop = $shop;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $this->user->dislikedShops()->detach($this->shop);
    }
}
