<?php

namespace App\Http\Controllers;

use App\Jobs\DeleteShopFromDislike;
use App\Shop;
use Illuminate\Http\Request;

class ShopController extends Controller
{
	/**
	 *
	 * Display Nearby Shops
	 *
	 */
    public function nearbyShops(){
    	return view('nearby-shops');
    }

    /**
	 *
	 * Display Prefered Shops
	 *
	 */
    public function preferedShops(){
        return view('prefered-shops');
    }

    /**
     *
     * @return Nearby Shops as json to api
     *
     */
    public function apiNearbyShops(){
    	$shops = auth()->user()->nearbyShops()->simplePaginate(50);
    	return response()->json($shops);
    }

    /**
     *
     * @return Prefered Shops as json to api
     *
     */
    public function apiPreferedShops(){
        $shops = auth()->user()->preferedShops()->simplePaginate(50);
        return response()->json($shops);
    }

    /**
     *
     * Like a shop by user
     *
     */
    public function apiLikeShop(Request $request){
    	$success = true;
    	try {
    		$shop = Shop::findOrFail($request->id);
    		auth()->user()->preferedShops()->attach($shop);
    	} catch (Exception $e) {
    		$success = false;
    	}
    	
    	return response()->json($success);
    }

    /**
     *
     * Dislike a shop by user
     *
     */
    public function apiDislikeShop(Request $request){
        $success = true;
        try {
            $shop = Shop::findOrFail($request->id);
            auth()->user()->dislikedShops()->attach($shop);

            // Add a queue job to jobs table, and run it after 2 hours. The job is : detach shop from user.
            DeleteShopFromDislike::dispatch(auth()->user(), $shop)->delay(now()->addHours(2));
            
        } catch (Exception $e) {
            $success = false;
        }
        
        return response()->json($success);
    }

    /**
     *
     * Unlike a shop by user
     *
     */
    public function apiUnlikeShop(Request $request){
        $success = true;
        try {
            $shop = Shop::findOrFail($request->id);
            auth()->user()->preferedShops()->detach($shop);
        } catch (Exception $e) {
            $success = false;
        }
        
        return response()->json($success);
    }
}
