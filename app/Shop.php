<?php

namespace App;

//use Illuminate\Database\Eloquent\Model;
use App\User;
use Jenssegers\Mongodb\Eloquent\Model;

class Shop extends Model
{
    protected $fillable = [
        'picture', 'name', 'email', 'city', 'location'
    ];

    public function dislikedByUser(){
    	return $this->belongsToMany(User::class, null, 'dislikedshops', 'dislikedby');
    }

    public function preferedByUser(){
    	return $this->belongsToMany(User::class, null, 'preferedshops', 'preferedby');
    }
}
