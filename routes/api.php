<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

Route::group(['middleware' => 'auth:api'], function(){
	// Routes to get shops
	Route::get('get-nearby-shops', 'ShopController@apiNearbyShops')->name('api-get-nearby-shops');
	Route::get('get-prefered-shops', 'ShopController@apiPreferedShops')->name('api-get-prefered-shops');

	// Routes to do actions
	Route::post('like-shop', 'ShopController@apiLikeShop')->name('api-like-shop');
	Route::post('dislike-shop', 'ShopController@apiDislikeShop')->name('api-dislike-shop');
	Route::post('unlike-shop', 'ShopController@apiUnlikeShop')->name('api-unlike-shop');
});