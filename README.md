# Nearby Shops

Nearby Shops is a web application that display all nearby shops based on your location.

# New Features!

  - Like/Unlike a shop;
  - Display prefered shops;
  - Dislike a shop => Do not display for the next 2 hours;

### Tech

Nearby Shops use some open source code :

* [Laravel] - Laravel is a free, open-source PHP web framework.
* [MongoDB] - MongoDB is a cross-platform document-oriented database program.
* [Vue.js] - Vue.js is an open-source JavaScript framework for building user interfaces and single-page applications.
* [Twitter Bootstrap] - great UI boilerplate for modern web apps
* [Webpack] - Webpack is an open-source JavaScript module bundler.
* [Yarn] - A package manager for JavaScript.

### Installation

Nearby Shops requires [MongoDB](https://www.mongodb.com/) to run.

Clone repository, install the dependencies then run config command :

```sh
$ git clone https://bitbucket.org/wadierohan/united-remote.git
$ cd united-remote
$ composer install
$ php artisan config:app && php artisan config:db
```

But if you are not a lazy person, here are all steps to follow :

```sh
$ git clone https://bitbucket.org/wadierohan/united-remote.git
$ cd united-remote
$ composer install
$ cp .env.example .env #Copy .env.example to .env
$ nano .env #Edit .env file to fit your configuration. PS : Just leave App_Key blank
$ php artisan key:generate #Here we fill App_Key
$ php artisan config:cache #Some servers cache need the hard way :p .. clear config cache
$ php artisan migrate
$ php artisan db:seed
$ php artisan fix:passport #Fix passport compatibility with mongodb
$ php artisan passport:install
```

### Running

Once everything is ready, there is 2 commands to run :
```sh
$ php artisan serv #To Run Built-in php server (This command can be ignored if you have a web server)
$ php artisan queue:work --tries=3 #To watch any disliked Shops so it can be restored after 2 hours
```

### Todos

 - Display Shops on map
 - Add a Shop rate based on prefered by users
 
**A huge thanks to [United Remote](https://unitedremote.com) for this amazing challenge**
**Wadie ELARRIM**

   [Laravel]: <https://laravel.com>
   [MongoDB]: <https://www.mongodb.com>
   [Vue.js]: <https://vuejs.org>
   [Twitter Bootstrap]: <https://getbootstrap.com>
   [Webpack]: <https://webpack.js.org>
   [Yarn]: <https://yarnpkg.com>